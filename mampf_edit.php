
<?php
$gui_data = array();

include "db/mampf-form-check.inc.php";

if (array_key_exists('mampf_default', $gui_data)) {
    # es wird ein neues Mampf angelegt
    $mampf = $gui_data["mampf_default"];
} else {
    trigger_error("mampf_default ist nicht gesetzt!", E_USER_ERROR);
}

$gui_data["edit_mampf"] = $edit_mampf;
$gui_data["mampf"] = $mampf;
$gui_data["mampf"]["id"] = $mampf_id;

array_walk_recursive($gui_data, function (&$item){$item = htmlentities($item);});
include "template/edit.inc.php";
