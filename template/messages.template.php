<html>
<?php include "components/head.inc.php"?>
<body  class="mdl-color--grey-100">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <?php include "components/header.inc.php"?>
    <?php include "components/drawer.inc.php"?>
    <main class="mdl-layout__content">
        <?php include "components/message.inc.php"?>
    </main>
</div>
</body>
</html>
