<html>
<?php include "components/head.inc.php" ?>
<body class="mdl-color--grey-100">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <?php include "components/header.inc.php" ?>
    <?php include "components/drawer.inc.php" ?>
    <main class="mdl-layout__content">
        <?php include "components/message.inc.php" ?>

        <form action="user_add.php" method="POST">
            <div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid mampf">

                <div class="mdl-card__title mdl-color-text--primary">
                    <h2 class="mdl-card__title-text">
                        Benutzer hinzufügen
                    </h2>
                </div>
                <div class="mdl-card__supporting-text">
                    <p>Bevor du dich als Mitesser eintragen kannst, wähle bitte einen Namen</p>
                    <p>Du darst dir auch gerne verschiedene Namen benutzen. Wichtig ist nur, dass dein Name eindeutig
                        ist.</p>
                </div>

                <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
                    <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                        <input class="mdl-textfield__input" type="text" name="name" id="name">
                        <label class="mdl-textfield__label" for="name">Benutzername</label>
                    </div>
                </div>
                <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">

                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-1">
                        <input type="checkbox" id="checkbox-1" class="mdl-checkbox__input" checked>
                        <div class="mdl-checkbox__label">Hiermit bestätige ich, dass ich die <a href="gebote.php">10 Mampf Gebote</a> gelesen habe. </div>
                    </label>
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-2">
                        <input type="checkbox" id="checkbox-2" class="mdl-checkbox__input" checked>
                        <span class="mdl-checkbox__label">Hiermit bestätige ich, dass ich die <a href="gebote.php">10 Mampf Gebote</a> verstanden habe. </span>
                    </label>
                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-3">
                        <input type="checkbox" id="checkbox-3" class="mdl-checkbox__input" checked>
                        <span class="mdl-checkbox__label">Es gibt nur einen Togg und Chef ist sein Prophet. </span>
                    </label>
                </div>

                <button class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent "
                        type="submit">
                    Add Now !
                </button>
            </div>
        </form>
    </main>
</div>
</body>
</html>
