
<div class=" mdl-shadow--4dp mdl-cell mdl-card relique">

    <div class="mdl-card__title ">
        <h2 class="mdl-card__title-text"><?php echo $rel["name"] ?></h2>
    </div>
    <div class="mdl-card__menu">
        <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-button--raised"
           title="vergrößern"
           href="<?php echo $rel["path"] ?>">
            <i class="material-icons">zoom_in</i>
        </a>
    </div>

    <img src="<?php echo $rel["path"] ?>">
</div>