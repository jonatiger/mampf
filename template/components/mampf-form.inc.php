<form action="/mampf_edit.php" method="post">
    <input type="hidden" name="form_executed" id="form_executed" value="1">
    <input type="hidden" name="mampf_id" id="mampf_id" value="<?php echo $gui_data["mampf"]["id"] ?>">

    <div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid mampf">

        <div class="mdl-card__title mdl-color-text--primary">
            <h2 class="mdl-card__title-text">
				<?php if ($gui_data["edit_mampf"]): ?>
                    Mampf editieren
				<?php else: ?>
                    Neues Mampf anlegen
				<?php endif; ?>
            </h2>
        </div>

        <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="main_dish" id=="main_dish"
                       value="<?php echo $gui_data["mampf_default"]["main_dish"] ?>">

                <label class="mdl-textfield__label" for="main_dish">Hauptgericht</label>
            </div>
            <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="side_dish" id="side_dish"
                       value="<?php echo $gui_data["mampf_default"]["side_dish"] ?>">
                <label class="mdl-textfield__label" for="side_dish">Beilage</label>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="pre_dish" id="pre_dish"
                       value="<?php echo $gui_data["mampf_default"]["pre_dish"] ?>">
                <label class="mdl-textfield__label" for="pre_dish">Vorspeise</label>
            </div>
            <div class="mdl-cell mdl-cell--3-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="dessert" id="dessert"
                       value="<?php echo $gui_data["mampf_default"]["dessert"] ?>">
                <label class="mdl-textfield__label" for="dessert">Nachspeise</label>
            </div>
            <label class="mdl-cell mdl-cell--6-col mdl-switch mdl-js-switch mdl-js-ripple-effect" for="veggi">
                <input type="checkbox" id="veggi" name="veggi" class="mdl-switch__input"
					<?php if ($gui_data["mampf_default"]["veggi"]) echo "checked" ?>
                >
                <span class="mdl-switch__label">Veggi/Halal möglich</span>
            </label>

            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <textarea class="mdl-textfield__input" type="text" rows="3"
                          name="comment"
                          id="dish_comment"><?php echo $gui_data["mampf_default"]["comment"] ?></textarea>
                <label class="mdl-textfield__label" for="dish_comment">Anmerkung</label>
            </div>
        </div>

        <div class="mdl-cell mdl-cell--6-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <select class="mdl-textfield__input" id="cook" name="cook">
                    <option></option>
					<?php foreach ($gui_data["users"] as $user): ?>
                        <option value="<?php echo $user["id"]; ?>"
							<?php
							if ($user["id"] == $gui_data["mampf"]["cook_id"]) {
								echo " selected='true' ";
							}
							?>>
							<?php echo $user["name"]; ?>
                        </option>
					<?php endforeach; ?>
                </select>
                <label class="mdl-textfield__label" for="octane">Koch</label>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" name="place" id="place"
                       value="<?php echo $gui_data["mampf_default"]["location"] ?>">
                <label class="mdl-textfield__label" for="place">Ort</label>
            </div>
        </div>

        <div class="mdl-cell mdl-cell--6-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                <input class="mdl-textfield__input" type="date" name="date" id="date"
                       value="<?php echo $gui_data["mampf_default"]["date"]; ?>">
                <label class="mdl-textfield__label" for="date">Datum</label>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                <input class="mdl-textfield__input" type="time" name="time" id="time" step="1"
                       value="<?php echo $gui_data["mampf_default"]["time"] ?>">
                <label class="mdl-textfield__label" for="time">Zeit</label>
            </div>
        </div>

        <div class="mdl-cell mdl-cell--6-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <span>Anmeldung bis:</span>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                <input class="mdl-textfield__input" type="date" name="deadline_date" id="deadline_date"
                       value="<?php echo $gui_data["mampf_default"]["deadline_date"]; ?>">
                <label class="mdl-textfield__label" for="deadline_date">Datum</label>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                <input class="mdl-textfield__input" type="time" name="deadline_time" id="deadline_time" step="1"
                       value="<?php echo $gui_data["mampf_default"]["deadline_time"] ?>">
                <label class="mdl-textfield__label" for="deadline_time">Zeit</label>
            </div>
        </div>

        <div class="mdl-cell mdl-cell--6-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" name="max_guest"
                       id="sample4"
                       value="<?php echo $gui_data["mampf_default"]["max_guest"] ?>">
                <label class="mdl-textfield__label" for="sample4">Anzahl Gäste</label>
                <span class="mdl-textfield__error">Input is not a number!</span>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                <input class="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" name="max_helfer"
                       id="sample4"
                       value="<?php echo $gui_data["mampf_default"]["max_helfer"] ?>">
                <label class="mdl-textfield__label" for="sample4">Anzahl Helfer</label>
                <span class="mdl-textfield__error">Input is not a number!</span>
            </div>
        </div>

        <button class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent "
                type="submit">
			<?php if ($gui_data["edit_mampf"]): ?>
                Änderungen speichern!
			<?php else: ?>
                Neues Mampf Now!
			<?php endif; ?>
        </button>


    </div>
</form>
