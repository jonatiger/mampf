<header class="mdl-layout__header mdl-layout__header--waterfall">
    <div class="mdl-layout__header-row ">
        <span class="mdl-layout-title">Mampf</span>
        <div class="mdl-layout-spacer"></div>
    </div>

    <?php
    if (isset($gui_data["mampf_search"]["startdate"])):
        ?>
        <div class="mdl-layout__header-row sub_header mdl-color--primary-dark mdl-typography--text-right">
            <div class="mdl-layout-spacer"></div>
            <form class="header_input mdl-grid--no-spacing mdl-grid mdl-cell--8-col  mdl-cell--4-col-phone" action="/home.php" method="post">
                <span class=" mdl-cell mdl-cell--3-col mdl-cell--1-col-phone mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                    <input  class="mdl-textfield__input" type="date" name="startdate" id="start_date"
                           value="<?php echo $gui_data["mampf_search"]["startdate"]; ?>">
                    <label class="mdl-textfield__label mdl-color-text--primary-contrast" for="date">von ... </label>
                </span>
                <span class="  mdl-cell mdl-cell--3-col mdl-cell--1-col-phone mdl-textfield mdl-js-textfield mdl-textfield--floating-label float-always">
                    <input class="mdl-textfield__input" type="date" name="enddate" id="end_date"
                           value="<?php echo $gui_data["mampf_search"]["enddate"]; ?>">
                    <label class="mdl-textfield__label mdl-color-text--primary-contrast" for="date">bis ...</label>
                </span>
                <div class="mdl-cell mdl-cell--1-col mdl-grid">
                    <button class="mdl-cell mdl-cell--1-col mdl-button--icon header_input mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect"
                            type="submit">
                        <i class="material-icons">search</i>
                    </button>
                </div>
            </form>
        </div>
    <?php endif; ?>

</header>