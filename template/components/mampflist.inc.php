<?php
function truncate($text, $chars = 25)
{
	if (strlen($text) <= $chars) {
		return $text;
	}
	return substr($text, 0, $chars - 3) . " <span class='mdl-button--raised'>...</span>";
}

?>
<?php foreach ($gui_data["mampflist"] as $mampf): ?>
    <div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid mampf">

        <div class="mdl-card__title mdl-color-text--primary">
            <h2 class="mdl-card__title-text">Mampf am
                &nbsp;<strong><?php echo $mampf["date"]; ?></strong>
                &nbsp; um &nbsp;<strong><?php echo $mampf["time"]; ?></strong> &nbsp;Uhr
            </h2>
        </div>
        <div class="mdl-grid mdl-card__actions mdl-card--border">

            <a class="mdl-cell mdl-cell--4-col mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect
                <?php
			$has_buyer = false;
			foreach ($mampf["guests"] as $guest) {
				if ($guest["is_buyer"]) {
					$has_buyer = true;
					break;
				}
			}
			if (!$has_buyer) {
				echo " mdl-button--accent mdl-button--raised ";
			}
			?>"
               href="/guest_edit.php?is_new&is_buyer&mampf_id=<?php echo $mampf["id"] ?>">
                Als Einkäufer eintragen
            </a>
            <a class=" mdl-cell mdl-cell--4-col mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect
			<?php
			$has_helper = false;
			foreach ($mampf["guests"] as $guest) {
				if ($guest["is_helper"]) {
					$has_helper = true;
					break;
				}
			}
			if (!$has_helper) {
				echo " mdl-button--accent mdl-button--raised ";
			}
			?>"
               href="/guest_edit.php?is_new&is_helper&mampf_id=<?php echo $mampf["id"] ?>">
                Als Helfer eintragen
            </a>
            <a class=" mdl-cell mdl-cell--4-col mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect <?php
			if (!$mampf["food"]["dessert"]) {
				echo " mdl-button--raised ";
			}
			?>"
               href="/guest_edit.php?is_new&is_dessert&mampf_id=<?php echo $mampf["id"] ?>">
                Nachtisch machen
            </a>
            <a class="mdl-cell mdl-cell--4-col mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect<?php
			if (count($mampf["guests"]) < $mampf["max_guest"]) {
				echo " mdl-button--raised ";
			}
			?>"
               href="/guest_edit.php?is_new&mampf_id=<?php echo $mampf["id"] ?>">
                Als Gast eintragen
            </a>
        </div>
        <div class="mdl-card__menu">
            <a class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect"
               title="Dieses Mampf editieren"
               href="mampf_edit.php?mampf_id=<?php echo $mampf["id"] ?>">
                <i class="material-icons">edit</i>
            </a>
        </div>
        <ul class="mdl-cell mdl-list mdl-shadow--4dp mdl-cell--6-col">
            <li class="mdl-list__item  mdl-typography--font-black ">
                <span class="mdl-list__item-primary-content">Hauptspeise</span>
                <span class="mdl-list__item-secondary-content" title="<?php echo $mampf["food"]["main_dish"] ?>"><?php
					echo truncate($mampf["food"]["main_dish"])
					?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Beilage</span>
                <span class="mdl-list__item-secondary-content"
                      title="<?php echo $mampf["food"]["side_dish"] ?>"><?php echo truncate($mampf["food"]["side_dish"]) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Vorspeise</span>
                <span class="mdl-list__item-secondary-content"
                      title="<?php echo $mampf["food"]["pre_dish"] ?>"><?php echo truncate($mampf["food"]["pre_dish"]) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Nachspeise</span>
                <span class="mdl-list__item-secondary-content"
                      title="<?php echo $mampf["food"]["dessert"] ?>"><?php echo truncate($mampf["food"]["dessert"]) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Veggi/Halal möglich</span>
                <span class="mdl-list__item-secondary-content">
										<?php if ($mampf["veggi"]): ?>
                                            <i class="material-icons">done</i>
										<?php else: ?>
                                            <i class="material-icons">clear</i>
										<?php endif ?>
								</span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Anmerkung</span>
                <span class="mdl-list__item-secondary-content" title="<?php echo $mampf["food"]["comment"] ?>">
                    <?php
					echo truncate($mampf["food"]["comment"], 120) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Anmeldung bis</span>
                <span class="mdl-list__item-secondary-content"
                      title="<?php echo $mampf["deadline"] ?>"><?php echo truncate($mampf["deadline"]) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Ort</span>
                <span class="mdl-list__item-secondary-content"
                      title="<?php echo $mampf["location"] ?>"><?php echo truncate($mampf["location"]) ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Max Gäste</span>
                <span class="mdl-list__item-secondary-content"><?php echo $mampf["max_guest"] ?></span>
            </li>
            <li class="mdl-list__item">
                <span class="mdl-list__item-primary-content">Beitrag</span>
                <span class="mdl-list__item-secondary-content"><?php echo $mampf["fee"] ?></span>
            </li>
        </ul>
        <div class="mdl-cell mdl-cell--6-col mdl-grid mdl-shadow--4dp table-holder">
            <table class="mdl-data-table mdl-js-data-table mdl-cell mdl-cell--12-col">
                <thead>
                <tr>
                    <th class=""></th>
                    <th class=""></th>
                    <th class="mdl-data-table__cell--non-numeric">Name</th>
                    <th class="mdl-data-table__cell--non-numeric">Bemerkung</th>
                    <th class="">Auslagen</th>
                    <th class=""></th>
                </tr>
                </thead>
                <tbody>
				<?php foreach ($mampf["guests"] as $guest): ?>
                    <tr>
                        <td title="<?php echo $guest["no"] ?>"><?php echo $guest["no"] ?></td>
                        <td>
							<?php if ($guest["is_cook"]): ?>
                                <i class="material-icons" title="Koch">restaurant_menu</i>
							<?php endif ?>
							<?php if ($guest["is_buyer"]): ?>
                                <i class="material-icons" title="Einkäufer">shopping_cart</i>
							<?php endif ?>
							<?php if ($guest["is_helper"]): ?>
                                <i class="material-icons" title="Helfer">accessibility</i>
							<?php endif ?>
							<?php if ($guest["is_dessert"]): ?>
                                <i class="material-icons" title="Dessert">cake</i>
							<?php endif ?>
                        </td>
                        <td title="<?php echo $guest["name"] ?>"><?php echo truncate($guest["name"]) ?></td>
                        <td title="<?php echo $guest["comment"] ?>"><?php
							echo truncate($guest["comment"]) ?></td>
                        <td><?php
							if ($guest["auslage"] != 0) {
								echo number_format($guest["auslage"], 2, ",", ".") . "€";
							}
							?></td>
                        <td>
                            <a class="mdl-button mdl-button--raised mdl-button--icon mdl-js-button mdl-js-ripple-effect"
                               href="guest_edit.php?mampf_id=<?php
							   echo $mampf["id"];
							   echo "&guest_id=";
							   echo $guest["id"];
							   ?>">
                                <i class="material-icons">edit</i>
                            </a>
                        </td>

                    </tr>
				<?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>

<?php endforeach ?>
