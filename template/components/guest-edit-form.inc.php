<form action="/guest_edit.php" method="post">
    <input type="hidden" name="mampf_id" value="<?php echo $gui_data["user_default"]["mampf_id"] ?>">
    <input type="hidden" name="guest_id" value="<?php echo $gui_data["user_default"]["guest_id"] ?>">
    <input type="hidden" name="is_new" value="<?php echo $gui_data["user_default"]["is_new"] ?>">
    <input type="hidden" name="form_processed" value="1">

    <div class=" mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid mampf">

        <div class="mdl-card__title mdl-color-text--primary">
            <h2 class="mdl-card__title-text">
                <?php if ($gui_data["is_new"]): ?>
                    Mitesser hinzufügen
                <?php else: ?>
                    Mitesser editieren
                <?php endif; ?>
            </h2>
        </div>
        <div class="mdl-card__supporting-text">
            <?php if ($gui_data["is_new"]): ?>
                <p>Wähle deinen Mampf-Namen aus der Liste aus, um dich für das Mampf am
                    <?php echo date('d.m.Y', strtotime($gui_data["mampf_date"])) ?> anzumelden. </p>
            <?php else: ?>
                <p>Mache die gewünschten Änderungen für das Mampf am
                    <?php echo date('d.m.Y', strtotime($gui_data["mampf_date"])) ?>. </p>
            <?php endif; ?>
        </div>


        <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <select class="mdl-textfield__input" id="guest" name="guest_id" <?php
                if (!$gui_data["is_new"]) echo "disabled"; ?>
                >
                    <option></option>
                    <?php if ($gui_data["create_user"]): ?>
                        <option value="<?php echo $gui_data["user_default"]["guest_id"] ?>" selected="true">
                            <?php echo $gui_data["new_user_name"] ?>
                        </option>
                    <?php endif; ?>

                    <?php foreach ($gui_data["users"] as $user): ?>
                        <option value="<?php echo $user["id"] ?>"
                            <?php
                            if (!$gui_data["is_new"] && ($user["id"] == $gui_data["user_default"]["guest_id"])) {
                                echo " selected='true' ";
                            }
                            ?>>
                            <?php echo $user["name"] ?>
                        </option>
                    <?php endforeach; ?>
                </select>
                <label class="mdl-textfield__label" for="octane">Mampfer Name</label>
            </div>

            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="place" name="comment" value="<?php echo $gui_data["user_default"]["comment"]; ?>">
                <label class="mdl-textfield__label" for="place">Bemerkung</label>
            </div>
        </div>
        <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
            <label class="mdl-cell mdl-cell--6-col mdl-switch mdl-js-switch mdl-js-ripple-effect"
                   for="switch-1">
                <input type="checkbox" id="switch-1" name="isEinkauf" class="mdl-switch__input"
                    <?php if ($gui_data["user_default"]["isEinkauf"]) echo "checked" ?>
                >
                <span class="mdl-switch__label">Einkäufer werden</span>
            </label>
            <label class="mdl-cell mdl-cell--6-col mdl-switch mdl-js-switch mdl-js-ripple-effect"
                   for="switch-2">
                <input type="checkbox" id="switch-2" name="isHelfer" class="mdl-switch__input"
                    <?php if ($gui_data["user_default"]["isHelfer"]) echo "checked" ?>
                >
                <span class="mdl-switch__label">Helfer werden</span>
            </label>
            <label class="mdl-cell mdl-cell--6-col mdl-switch mdl-js-switch mdl-js-ripple-effect"
                   for="switch-3">
                <input type="checkbox" id="switch-3" name="isDessert" class="mdl-switch__input"
                    <?php if ($gui_data["user_default"]["isDessert"]) echo "checked" ?>
                >
                <span class="mdl-switch__label">Nachtisch machen</span>
            </label>
            <label class="mdl-cell mdl-cell--6-col mdl-switch mdl-js-switch mdl-js-ripple-effect"
                   for="switch-4">
                <input type="checkbox" id="switch-4" name="isKoch" class="mdl-switch__input"
                    <?php if ($gui_data["user_default"]["isKoch"]) echo "checked" ?>
                >
                <span class="mdl-switch__label">Koch werden</span>
            </label>
        </div>

        <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col">
                <p>
                    Warst du Einkaufen, oder hattest andere Ausgabem? Dann trage deine kosten hier ein.
                </p>
                <p>
                    <i>Protipp: </i> Du kannst hier auch noch nach dem Mampf was eintragen.
                </p>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="number" id="auslage" name="auslage" step="0.01" value="<?php echo $gui_data["user_default"]["auslage"] ?>">
                <label class="mdl-textfield__label" for="auslage">Auslage(Euro)</label>
            </div>
        </div>
        <button class="mdl-cell mdl-cell--12-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent "
                type="submit">
            <?php if ($gui_data["is_new"]): ?>
                !!!Ich will mitessen ! ! !
            <?php else: ?>
                Änderung speichern
            <?php endif; ?>
        </button>

        <?php if (!$gui_data["is_new"]): ?>
            <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
                <a class="mdl-cell mdl-cell--6-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored "
                   type="submit" href="guest_remove.php?mampf_id=<?php echo $gui_data["user_default"]["mampf_id"]; ?>&guest_id=<?php echo $gui_data["user_default"]["guest_id"]; ?>">
                    Von diesem Mampf abmelden!
                </a>
            </div>
        <?php endif; ?>
    </div>
</form>

<?php if ($gui_data["is_new"]): ?>
    <form action="/guest_edit.php?create_user" method="post">
        <input type="hidden" name="mampf_id" value="<?php echo $gui_data["user_default"]["mampf_id"] ?>">
        <input type="hidden" name="guest_id" value="<?php echo $gui_data["user_default"]["guest_id"] ?>">
        <input type="hidden" name="is_new" value="<?php echo $gui_data["user_default"]["is_new"] ?>">
        <input type="hidden" name="form_processed" value="1">
        <input type="hidden" name="create_user" value="1">

        <div class="mdl-cell mdl-cell--12-col mdl-grid mdl-shadow--4dp">
            <div class="mdl-cell mdl-cell--6-col">
                <p>
                    Zum ersten Mal dabei? Werde Esser, damit dein Name oben in der Liste auftaucht
                </p>
            </div>
            <div class="mdl-cell mdl-cell--6-col">
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="new_user_name" name="new_user_name">
                <label class="mdl-textfield__label" for="new_user_name">Hier Namen einfügen</label>
            </div>
            <div class="mdl-cell mdl-cell--6-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                <input class="mdl-textfield__input" type="text" id="new_user_mail" name="new_user_mail">
                <label class="mdl-textfield__label" for="new_user_mail">Bitte email angeben</label>
            </div>
            <button class="mdl-cell mdl-cell--6-col mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent "
                    type="submit">
                Neuen Esser hinzufügen
            </button>
        </div>
 
    </form>
<?php endif; ?>