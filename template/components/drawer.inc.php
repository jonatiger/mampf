<div class="mdl-layout__drawer ">
    <span class="mdl-layout-title ">Menu</span>
    <nav class="mdl-navigation mdl-layout-spacer">

        <a class="mdl-navigation__link " href="/index.php">
            <i class="material-icons" role="presentation">nature</i> Willkommen
        </a>
        <a class="mdl-navigation__link " href="/home.php">
            <i class="material-icons" role="presentation">home</i> Home
        </a>

        <div class=""><hr></div>

        <a class="mdl-navigation__link " href="/mampf_edit.php">
            <i class="material-icons">restaurant</i> Neues Mampf
        </a>

        <a class="mdl-navigation__link " href="https://piratenpad.de/p/mampf">
            <i class="material-icons">speaker_notes</i> Notizen
        </a>
        <a class="mdl-navigation__link " href="/guthaben.php">
            <i class="material-icons" role="presentation">euro_symbol</i> Guthaben
        </a>

        <div class=""><hr></div>

        <a class="mdl-navigation__link " href="/gebote.php">
            <i class="material-icons" role="presentation">gavel</i> Gebote
        </a>
        <a class="mdl-navigation__link" href="/reliquien.php">
            <i class="material-icons">touch_app</i> Reliquien
        </a>
        <a class="mdl-navigation__link " href="http://mampf.hadiko.de">
            <i class="material-icons" role="presentation">highlight_off</i> Altes Wiki
        </a>
        <a class="mdl-navigation__link " href="http://mampf-dev.jonatiger.de/">
            <i class="material-icons" role="presentation">pie_chart</i> Testsystem
         </a>

        <div class=""><hr></div>

        <a class="mdl-navigation__link " href="https://gitlab.com/jonatiger/mampf" rel="noreferrer" target="_blank" style="white-space:nowrap">
            <i class="material-icons" role="presentation">code</i> Sourcecode auf GitLab
        </a>
    </nav>
</div>
