<div class="mdl-shadow--4dp mdl-cell mdl-card mdl-cell--12-col mdl-grid table-holder">
    <table class="mdl-data-table mdl-js-data-table mdl-cell  mdl-cell--12-col finance">
        <thead>
        <tr>
            <th class="mdl-data-table__cell--non-numeric">Name</th>
            <th class="mdl-data-table__cell--non-numeric">Guthaben</th>
            <th class=""># Helfer</th>
            <th class=""># Einkäufer</th>
            <th class=""># Mampfs</th>
            <th class="mdl-data-table__cell--non-numeric">Schuldet noch <br> X Nachtische</th>
            <th class="mdl-data-table__cell--non-numeric">Bemerkung</th>
            <th class="">Letzes <br>Mampf</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($gui_data["fin"] as $user): ?>
            <tr>
                <td><?php echo $user["name"]; ?> </td>
                <td><?php echo $user["balance"]; ?> </td>
                <td><?php echo $user["helfer_count"]; ?> </td>
                <td><?php echo $user["einkauf_count"]; ?> </td>
                <td><?php echo $user["mampf_count"]; ?> </td>
                <td><?php echo $user["dessert_due"]; ?> </td>
                <td><?php echo $user["comment"]; ?> </td>
                <td><?php echo $user["last_mampf"]; ?> </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>

</div>