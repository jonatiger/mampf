<head>
    <link rel="stylesheet" href="/static/mdl/material-bluegray-indigo.min.css"/>
    <script src="/static/mdl/material.min.js"></script>
    <link rel="stylesheet" href="/static/mdl/icons/icons.css">
    <style>
        .sub_header .mdl-layout__header-row .mdl-navigation__link:last-of-type  {
            padding-right: 0;
        }
        .sub_header{
            height: auto;
        }

        .mampf {
            /* max-width: 900px; */
        }

        .float-always .mdl-textfield__label {
            font-size: 12px;
            top: 4px;
        }

        .demo-avatar {
            width: 48px;
            height: 48px;
            border-radius: 24px;
        }

        .mdl-checkbox {
            /* bugfix. see https://github.com/google/material-design-lite/issues/1558#issuecomment-202872395 */
            height: auto
        }



        .relique.mdl-card {
            width: 320px;
            height: 320px;
        }
        .relique > .mdl-card__title {
            color: #fff;
            height: 100px;
            background:  pink;
        }

        .mdl-card {
             overflow: auto;
        }

        .gebot.mdl-card {
            overflow: hidden;
        }

        .table-holder{
            overflow: auto;
        }

        td,th{
            padding: 0px 6px !important;

        }
        .finance tbody tr:nth-child(2n) {
            background-color: whitesmoke;
        }
        .finance tbody tr:nth-child(2n+1) {
            background-color: white;
        }


    </style>

    <!-- MDL requires this. see https://stackoverflow.com/a/33303226/5132456 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
