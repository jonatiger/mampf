
<div class=" mdl-shadow--4dp mdl-cell mdl-card gebot ">
    <div class="mdl-card__title mdl-color--primary-dark mdl-color-text--primary-contrast ">
        <h2 class="mdl-card__title-text"><?php echo $num; ?>. Gebot</h2>
    </div>
    <div class="mdl-card__supporting-text">
        <h5><?php echo $text; ?></h5>
    </div>
</div>

