<html>
<?php include "components/head.inc.php" ?>
<body class="mdl-color--grey-100">
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <?php include "components/header.inc.php" ?>
    <?php include "components/drawer.inc.php" ?>
    <main class="mdl-layout__content mdl-grid mdl-cell--4-col-phone">

        <?php

        $rel = [
            "name" => "Togg's Latte",
            "path" => "/static/images/ttogs_latte.jpg",
        ];
        include "components/relique.inc.php";

        $rel = [
            "name" => "Togg's Gruß",
            "path" => "/static/images/gruss_von_ttog.png",
        ];
        include "components/relique.inc.php";

        $rel = [
            "name" => "Togg's Gebote",
            "path" => "/static/images/mampf_gebote_tafeln.JPG",
        ];
        include "components/relique.inc.php";


        $rel = [
            "name" => "Togg's Rezept",
            "path" => "/static/images/ttogs_lieblingsrezept.png",
        ];
        include "components/relique.inc.php";


        ?>
    </main>
</div>


</body>
</html>
