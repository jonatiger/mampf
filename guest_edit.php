<?php

if(!isset($gui_dat)){
    $gui_data = array();
}
$messages = [];

#basic variablen zuweisung
$create_user = isset($_GET["create_user"]);
$initial_call = !isset($_POST["form_processed"]);

if($initial_call){
    $is_new = isset($_GET["is_new"]);
    $mampf_id = $_GET["mampf_id"];
    if(!$is_new){
        $user_id = $_GET["guest_id"];
    }
} else{
    $is_new = $_POST["is_new"];
    $mampf_id = $_POST["mampf_id"];
    if(!$create_user){
        $user_id = $_POST["guest_id"];
    }
}

$gui_data["is_new"] = $is_new;
$gui_data["create_user"] = $create_user;

if($create_user){
    $gui_data["new_user_name"] = $_POST["new_user_name"];
}else{
    $gui_data["new_user_name"] = null;
}

include_once "db/default_user_val.inc.php";
include_once "db/user-form-check.inc.php";

array_walk_recursive($gui_data, function (&$item){$item = htmlentities($item);});
include "template/guest_edit.inc.php";
