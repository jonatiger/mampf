<?php
$messages = array();

$mampf_id = $_GET["mampf_id"];
$guest_id = $_GET["guest_id"];

include "db/db_change.inc.php";

if(remove_anmeldung($mampf_id, $guest_id)){
	array_push($messages, ["type" => "success", "text" => "Erfolgreich von diesem Mampf abgemeldet!"]);
} else{
	array_push($messages, ["type" => "error", "text" => "Fehler beim Abmelden von diesem Mampf!"]);
}

if(!isset($gui_data)){
	$gui_data = array();
}
$gui_data["messages"] = $messages;

array_walk_recursive($gui_data, function (&$item){$item = htmlentities($item);});
include "template/messages.template.php";

?>
