<?php
include_once "sql.inc.php";

if(isset($gui_data) == false){
    $gui_data = array();
}
if($initial_call == false){
    #previous val
    $gui_data["mampf_default"]["date"] = $_POST["date"];
    $gui_data["mampf_default"]["time"] = $_POST["time"];
    $gui_data["mampf_default"]["created_date"] = null;
    $gui_data["mampf_default"]["deadline_date"] = $_POST["deadline_date"];
    $gui_data["mampf_default"]["deadline_time"] = $_POST["deadline_time"];
    $gui_data["mampf_default"]["max_guest"] = $_POST["max_guest"];
    $gui_data["mampf_default"]["location"] = $_POST["place"];
    $gui_data["mampf_default"]["main_dish"] = $_POST["main_dish"];
    $gui_data["mampf_default"]["side_dish"] = $_POST["side_dish"];
    $gui_data["mampf_default"]["pre_dish"] = $_POST["pre_dish"];
    $gui_data["mampf_default"]["dessert"] = $_POST["dessert"];
    $gui_data["mampf_default"]["comment"] = $_POST["comment"];
    $gui_data["mampf_default"]["cook_id"] = $_POST["cook"];
    $gui_data["mampf_default"]["veggi"] = (int)isset($_POST["veggi"]);
    $gui_data["mampf_default"]["max_helfer"] = $_POST["max_helfer"];
} elseif($mampf_id > 0){
    #daten aus db
    $mampf = mysqli_fetch_array(get_mampf_data($mampf_id));
    $gui_data["mampf_default"]["date"] = $mampf["datum"];
    $gui_data["mampf_default"]["time"] = $mampf["uhrzeit"];
    $gui_data["mampf_default"]["created_date"] = $mampf["lastChange"];
    $gui_data["mampf_default"]["deadline_date"] = date("Y-m-d", strtotime($mampf["deadline"]));
    $gui_data["mampf_default"]["deadline_time"] = date("H:i:s", strtotime($mampf["deadline"]));
    $gui_data["mampf_default"]["max_guest"] = $mampf["max_guest"];
    $gui_data["mampf_default"]["location"] = $mampf["ort"];
    $gui_data["mampf_default"]["main_dish"] = $mampf["gericht"];
    $gui_data["mampf_default"]["side_dish"] = $mampf["beilage"];
    $gui_data["mampf_default"]["pre_dish"] = $mampf["vorspeise"];
    $gui_data["mampf_default"]["dessert"] = $mampf["dessert"];
    $gui_data["mampf_default"]["comment"] = $mampf["kommentar"];
    $gui_data["mampf_default"]["cook_id"] = $mampf["koch_user_id"];
    $gui_data["mampf_default"]["cook_name"] = $mampf["koch_name"];
    $gui_data["mampf_default"]["veggi"] = $mampf["veggi"];
    $gui_data["mampf_default"]["max_helfer"] = $mampf["max_helfer"];
} else{
    #default
    $gui_data["mampf_default"]["date"] = date('Y-m-d',strtotime(7-date('w') . ' days'));
    $gui_data["mampf_default"]["time"] = "19:00:00";
    $gui_data["mampf_default"]["created_date"] = null;
    $gui_data["mampf_default"]["deadline_date"] = date('Y-m-d',strtotime(5-date('w') . ' days'));
    $gui_data["mampf_default"]["deadline_time"] = '23:59:59';
    $gui_data["mampf_default"]["max_guest"] = 12;
    $gui_data["mampf_default"]["location"] = "F4";
    $gui_data["mampf_default"]["main_dish"] = null;
    $gui_data["mampf_default"]["side_dish"] = null;
    $gui_data["mampf_default"]["pre_dish"] = null;
    $gui_data["mampf_default"]["dessert"] = null;
    $gui_data["mampf_default"]["comment"] = null;
    $gui_data["mampf_default"]["cook_name"] = "TTOG";
    $gui_data["mampf_default"]["cook_id"] = 2;
    $gui_data["mampf_default"]["veggi"] = 0;
    $gui_data["mampf_default"]["max_helfer"] = 1;
}

$user_nbr = 0;
$result = get_all_user();
while($row = mysqli_fetch_array($result)){
    $user_nbr += 1;
    $gui_data["users"][$user_nbr]["name"] = $row["user_name"];
    $gui_data["users"][$user_nbr]["id"] = $row["user_id"];
}
?>
