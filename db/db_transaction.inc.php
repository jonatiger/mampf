<?php
include_once "db_connect.inc.php";

function write_transaction($datum, $mampf_id, $haben_kto, $soll_kto, $betrag, $desc){
	if($datum>'2018-01-01'){
		if($mampf_id == ""){
			$mampf_id = "Null";
		}
		$sql_str = "INSERT INTO t_transaction(datum,mampf_id,haben_konto,soll_konto,betrag,`desc`)
			VALUES('" . $datum . "'," . $mampf_id . "," . $haben_kto . "," . $soll_kto . "," . $betrag . ",'" . $desc . "')";
		return RunQuery($sql_str);
	}
}

function delete_transaction($param){
	#param is like param[x][field] = mampf_id and param[x][value] = 1
	$sql_str = "DELETE FROM t_transaction WHERE datum > '2018-01-01'";

	$i = 1;
	while(isset($param[$i])){
		$sql_str .= " AND " . $param[$i]["field"] . " = '" . $param[$i]["value"] ."'";
		$i += 1;
	}
	return RunQuery($sql_str);
}

function calc_mampf_kosten($mampf_id){
	$sql_str = "SELECT sum(ROUND(ceil(auslage*2)/2,1)) FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
	return mysqli_fetch_array(CreateQuery($sql_str))[0];
}
/*
function set_mampf_kosten($mampf_id){
	$kosten = calc_mampf_kosten($mampf_id);
	$sql_str = "UPDATE t_mampf SET kosten = " . $kosten . " WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
	return RunQuery($sql_str);
}
*/
function calc_mampf_fee($mampf_id){
	$mampf_kosten = calc_mampf_kosten($mampf_id);

	$sql_str = "SELECT count(*) FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
	$mampf_gäste = mysqli_fetch_array(CreateQuery($sql_str))[0];

	$sql_str = "SELECT NumVal FROM t_para WHERE item = 'min_fee'";
	$min_fee = mysqli_fetch_array(CreateQuery($sql_str))[0];

	$sql_str = "SELECT NumVal FROM t_para WHERE item = 'max_fee'";
	$max_fee = mysqli_fetch_array(CreateQuery($sql_str))[0];

	$hyp_cost = round(ceil($mampf_kosten / $mampf_gäste*2)/2,1);

	if($hyp_cost > $max_fee){
		return $max_fee;
	} elseif ($hyp_cost < $min_fee) {
		return $min_fee;
	} else {
		return $hyp_cost;
	}
}

function clear_transactions($mampf_id){
	$param = array();
	$param[1]["field"] = "mampf_id";
	$param[1]["value"] = $mampf_id;
	return delete_transaction($param);
}

function calc_abrechnung($mampf_id){
	#mampf abrechnung nur für neue mampf 3 monate zurück
	$sql_str = "SELECT CASE WHEN
	  (SELECT datum FROM t_mampf WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1) > (SELECT DATE(StrVal) FROM t_para WHERE item = 'freeze_date')
	  AND
	  (SELECT datum FROM t_mampf WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1) > (SELECT date_add(current_date, interval -3 month))
	THEN 1 ELSE 0 END as is_not_frozen";

	$mampf_is_not_frozen = mysqli_fetch_array(CreateQuery($sql_str))[0];
	if($mampf_is_not_frozen == 1){
		clear_transactions($mampf_id);

		$mampf_fee = calc_mampf_fee($mampf_id);

		#write fee in mampf table
		$sql_str = "UPDATE t_mampf SET kosten = " . $mampf_fee . " WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
		RunQuery($sql_str);

		#create transactions
		$sql_str = "SELECT NumVal FROM t_para WHERE item = 'mampf_kto'";
		$mampf_kto = mysqli_fetch_array(CreateQuery($sql_str))[0];

		$sql_str = "SELECT m.datum, t.user_id, t.auslage, t.isEinkauf, t.isDessert FROM t_teilnehmer t INNER JOIN t_mampf m ON m.mampf_id = t.mampf_id AND m.isCurrent = 1 WHERE t.mampf_id = " . $mampf_id . " AND t.isCurrent = 1";
		$result = CreateQuery($sql_str);

		while($row = mysqli_fetch_array($result)){
			#Auslagen buchen
			$Auslage_final = $row["auslage"];
			if($row["auslage"]>0){
				if($row["isEinkauf"]){
					$desc = "Einkauf";
					$Auslage_final = ceil($Auslage_final);
				} elseif($row["isDessert"]){
					$desc = "Nachtisch";
					$Auslage_final = round(ceil($Auslage_final*2)/2,1);
				} else {
					$desc = "Auslage";
				}

				write_transaction($row["datum"], $mampf_id, $mampf_kto, $row["user_id"], $Auslage_final, $desc);
			}

			#teilnahme buchen
			write_transaction($row["datum"], $mampf_id, $row["user_id"], $mampf_kto, $mampf_fee, "mampf");
		}
	}
}

function get_auslage($mampf_id, $user_id){
	$sql_str = "SELECT sum(auslage) FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1 AND user_id = " . $user_id;
	return mysqli_fetch_array(CreateQuery($sql_str))[0];
}

?>
