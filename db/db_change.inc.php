<?php
include_once "db_connect.inc.php";

function get_special_guest($mampf_id, $db_field){
	$sql_str = "SELECT user_id,1 as x FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND " . $db_field . " = 1 AND isCurrent = 1";
	return mysqli_fetch_array(CreateQuery($sql_str))[0];
}

function add_anmeldung($data){
	//$guest_id, $comment, $isKoch, $isHelfer, $isEinkauf, $isDessert
	$sql_str = "INSERT INTO t_teilnehmer(mampf_id,user_id,kommentar,isKoch,isHelfer,isEinkauf,isDessert,createdate,isCurrent)
		VALUES(" . $data["mampf_id"] . "," . $data["guest_id"] . ",'" . $data["comment"] . "'," . $data["isKoch"] . "," . $data["isHelfer"] . "," . $data["isEinkauf"] . "," . $data["isDessert"] . ",now(),1)";
	#echo $sql_str;
	$InsertSuccess = RunQuery($sql_str);
	return $InsertSuccess;
}

function change_anmeldung($data){
	//historisation last record
	$sql_str = "SELECT max(id) as maxid FROM t_teilnehmer WHERE mampf_id = " . $data["mampf_id"] . " AND user_id = " . $data["guest_id"] ;
	$LastID = mysqli_fetch_array(CreateQuery($sql_str))[0];
	$sql_str = "UPDATE t_teilnehmer SET isCurrent = 0 WHERE id = " . $LastID ;
	$InsertSuccess = RunQuery($sql_str);

	//get creation time
	$sql_str = "SELECT createdate FROM t_teilnehmer WHERE id = " . $LastID ;
	$CreateDate = mysqli_fetch_array(CreateQuery($sql_str))[0];

	//add new record
	$sql_str = "INSERT INTO t_teilnehmer(mampf_id,user_id,kommentar,isKoch,isHelfer,isEinkauf,isDessert,auslage,createdate,isCurrent)
		VALUES(" . $data["mampf_id"] . "," . $data["guest_id"] . ",'" . $data["comment"] . "'," . $data["isKoch"] . "," . $data["isHelfer"] . "," . $data["isEinkauf"] . "," . $data["isDessert"] . "," . $data["auslage"] . ",'" . $CreateDate . "',1)";
	$InsertSuccess = RunQuery($sql_str);
	return $InsertSuccess;
}

function remove_anmeldung($mampf_id, $guest_id){

	$sql_str = "SELECT id FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND user_id = " . $guest_id . " AND isCurrent = 1 ";
	$LastID = mysqli_fetch_array(CreateQuery($sql_str))[0];
	$sql_str = "UPDATE t_teilnehmer SET isCurrent = 0,removedate = Now()
		WHERE mampf_id = " . $mampf_id . " AND user_id = " . $guest_id . " AND isCurrent = 1" ;
	$InsertSuccess = RunQuery($sql_str);
	return $InsertSuccess;
}

function change_user_att($mampf_id, $user_id, $attribut, $val){
		#check if user exist
		$sql_str = "SELECT count(*) as uexist FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND user_id = " . $user_id . " AND isCurrent = 1";
		$user_exist = mysqli_fetch_array(CreateQuery($sql_str))[0];

		if($user_exist == 1){

			$sql_str = "SELECT * FROM t_teilnehmer WHERE mampf_id = " . $mampf_id . " AND user_id = " . $user_id . " AND isCurrent";
			$old_record = mysqli_fetch_array(CreateQuery($sql_str));
			$new_data = array();
			#copy all values
			$new_data["mampf_id"] = $old_record["mampf_id"];
			$new_data["guest_id"] = $old_record["user_id"];
			$new_data["comment"] = $old_record["kommentar"];
			$new_data["isKoch"] = $old_record["isHelfer"];
			$new_data["isHelfer"] = $old_record["isHelfer"];
			$new_data["isEinkauf"] = $old_record["isEinkauf"];
			$new_data["isDessert"] = $old_record["isDessert"];
			#change selected value
			$new_data[$attribut] = (string)$val;
			return change_anmeldung($new_data);
		} else{
			$new_data["mampf_id"] = $mampf_id;
			$new_data["guest_id"] = $user_id;
			$new_data["comment"] = "";
			$new_data["isKoch"] = "0";
			$new_data["isHelfer"] = "0";
			$new_data["isEinkauf"] = "0";
			$new_data["isDessert"] = "0";
			#change selected value
			$new_data[$attribut] = (string)$val;
			return add_anmeldung($new_data);
		}
}

function add_mampf($data){
	//get next id
	$sql_str = "SELECT Max(mampf_id) FROM t_mampf";
	$NewID = mysqli_fetch_array(CreateQuery($sql_str))[0] + 1;

	$sql_str = "INSERT INTO t_mampf(mampf_id,datum,koch_user_id,ort,uhrzeit,max_guest,deadline,kommentar,vorspeise,gericht,beilage,
			dessert,veggi,max_helfer,isCurrent)
		VALUES(" . $NewID . ",'" . $data["datum"] . "'," . $data["koch_id"] . ",'" . $data["ort"] . "','" . $data["zeit"] . "'," . $data["max_guest"] .
			",'" . $data["deadline"] . "','" . $data["comment"] . "','" . $data["vorspeise"] . "','" . $data["gericht"] . "','" . $data["beilage"] .
			"','" . $data["dessert"] . "'," . $data["veggi"] . "," . $data["max_helfer"] . ",1)";

	$InsertSuccess = RunQuery($sql_str);
	If($InsertSuccess){
		#add cook as guest
		$cook_data = array();
		$cook_data["mampf_id"] = $NewID;
		$cook_data["guest_id"] = $data["koch_id"];
		$cook_data["comment"] = "";
		$cook_data["isKoch"] = "1";
		$cook_data["isHelfer"] = "0";
		$cook_data["isEinkauf"] = "0";
		$cook_data["isDessert"] = "0";
		$InsertCook = add_anmeldung($cook_data);
	}
	return $InsertSuccess;
}

function change_mampf($data){
	//historisation last record
	$sql_str = "SELECT max(id) as maxid FROM t_mampf WHERE mampf_id = " . $data["mampf_id"] ;

	$LastID = mysqli_fetch_array(CreateQuery($sql_str))[0];

	$sql_str = "UPDATE t_mampf SET isCurrent = 0 WHERE id = " . $LastID ;
	$InsertSuccess = RunQuery($sql_str);

	//insert new record
	$sql_str = "INSERT INTO t_mampf(mampf_id,datum,koch_user_id,ort,uhrzeit,max_guest,deadline,kommentar,vorspeise,gericht,beilage,
			dessert,veggi,max_helfer,isCurrent)
		VALUES(" . $data["mampf_id"] . ",'" . $data["datum"] . "'," . $data["koch_id"] . ",'" . $data["ort"] . "','" . $data["zeit"] . "'," .
			$data["max_guest"] . ",'" . $data["deadline"] . "','" . $data["comment"] . "','" . $data["vorspeise"] . "','" . $data["gericht"] . "','" .
			$data["beilage"] . "','" . $data["dessert"] ."'," . $data["veggi"] . "," . $data["max_helfer"] . ",1)";
	$InsertSuccess = RunQuery($sql_str);

	#change cook if changed
	$old_cook_id = get_special_guest($data["mampf_id"],"isKoch");
	if($old_cook_id != $data["koch_id"]){
		#change old cook status
		change_user_att($data["mampf_id"], $old_cook_id, "isKoch", 0);

		#add new cook
		change_user_att($data["mampf_id"], $data["koch_id"], "isKoch", 1);
	}
	return $InsertSuccess;
}

function create_user($user_name, $user_mail, &$ErrMsg){
	$sql_str = "SELECT count(*) FROM t_user WHERE user_name = '" . $user_name . "'";
	$user_exist = mysqli_fetch_array(CreateQuery($sql_str))[0];

	if(!$user_exist){
		$sql_str = "INSERT INTO t_user(user_name,user_status,user_mail,creation_date)
			VALUES('" . $user_name . "',1,'" . $user_mail . "',now())";
		$InsertSuccess = RunQuery($sql_str);
		if(!$InsertSuccess){
			$ErrMsg = "Ups! Da ist ein Fehler passiert!";
		}
		return $InsertSuccess;
	} else{
		$ErrMsg = "Dieser Name existiert schon!";
		return false;
	}
}

function get_user_id($user_name){
	$sql_str = "SELECT user_id FROM t_user WHERE user_name = '" . $user_name . "'";
	return mysqli_fetch_array(CreateQuery($sql_str))[0];
}



?>
