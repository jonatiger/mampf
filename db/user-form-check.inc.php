<?php

$gui_data["messages"] = array();

include_once "db/db_change.inc.php";
include_once "db/db_transaction.inc.php";
include_once "db/sql.inc.php";

if($create_user){
	if($_POST["new_user_name"] == ""){
		array_push($messages, ["type" => "error", "text" => "Kein Name angegeben"]);
	} else{
		$ErrMsg = "";
		$create_result = create_user($_POST["new_user_name"], $_POST["new_user_mail"], $ErrMsg);
		if($create_result){
			array_push($messages, ["type" => "success", "text" => "User erstellt! Du kannst dich jetzt anmelden!"]);
			$gui_data["user_default"]["guest_id"] = get_user_id($_POST["new_user_name"]);
		} else{
			array_push($messages, ["type" => "error", "text" => $ErrMsg]);
		}
	}
} elseif(!$initial_call){

	$data = array();
    $data["mampf_id"] = $_POST["mampf_id"];
    $data["guest_id"] = $_POST["guest_id"];
    $data["comment"] = $_POST["comment"];
    $data["isKoch"] = (int)isset($_POST["isKoch"]);
    $data["isHelfer"] = (int)isset($_POST["isHelfer"]);
    $data["isEinkauf"] = (int)isset($_POST["isEinkauf"]);
    $data["isDessert"] = (int)isset($_POST["isDessert"]);
    $data["auslage"] = $_POST["auslage"];

	#check if need to calc mampf abrechnung
	$auslage_change = $data["auslage"] <> get_auslage($data["mampf_id"],$data["guest_id"]);

	if($is_new){
		#check if user already subscribed
		$guest_exist = guest_exist($data["mampf_id"],$data["guest_id"]);
		$free_task = get_free_tasks($data["mampf_id"],$data["guest_id"]);
		if($guest_exist){
			array_push($messages, ["type" => "error", "text" => "Du bist schon angemeldet!"]);
		} elseif($data["isHelfer"] && $free_task["helfer"]){
			array_push($messages, ["type" => "error", "text" => "Es gibt schon einen Helfer!"]);
		} elseif($data["isKoch"] && $free_task["koch"]){
			array_push($messages, ["type" => "error", "text" => "Es gibt schon einen Koch!"]);
		} elseif($data["isEinkauf"] && $free_task["einkauf"]){
			array_push($messages, ["type" => "error", "text" => "Es gibt schon einen Einkäufer!"]);
		} elseif($data["isDessert"] && $free_task["dessert"]){
			array_push($messages, ["type" => "error", "text" => "Es gibt schon einen Nachtisch!"]);

		} else{
			#send changes to db
			$save_success = add_anmeldung($data);
			if($save_success){
				$is_new = false;
				$user_credit = get_user_credit($data["guest_id"]);
				if($user_credit < 5){
					array_push($messages, ["type" => "warning", "text" => "Achtung! Du hast kein Guthaben mehr! (" . $user_credit . " EUR)"]);
				}
				array_push($messages, ["type" => "success", "text" => "Du bist jetzt Esser bei diesem Mampf!!"]);
			} else{
				array_push($messages, ["type" => "error", "text" => "Ups! Da ist ein Fehler passiert!"]);
			}
		}
	} else{
		#send new user to db
		$save_success = change_anmeldung($data);
		$user_credit = get_user_credit($data["guest_id"]);
		if($user_credit < 5){
			array_push($messages, ["type" => "warning", "text" => "Achtung! Du hast kein Guthaben mehr! (" . $user_credit . " EUR)"]);
		}
		if($save_success){
			array_push($messages, ["type" => "success", "text" => "Die Änderungen wurden gespeichert!"]);
		} else{
			array_push($messages, ["type" => "error", "text" => "Ups! Da ist ein Fehler passiert!"]);
		}
	}

	#recalc mampf abrechnung if needed
	if($auslage_change){
		calc_abrechnung($data["mampf_id"]);
	}
}
if(deadline_expired($mampf_id) && $is_new){
	array_push($messages, ["type" => "warning", "text" => "Achtung! Die Anmeldefrist ist abgelaufen!"]);
}


$gui_data["messages"] = $messages;

?>
