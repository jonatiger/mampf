<?php

include_once "db_credentials.inc.php";


function new_mysqli(){
	global $db_credentials;
	$mysqli = new mysqli($db_credentials["server"], $db_credentials["uid"], $db_credentials["pw"], $db_credentials["db"]);
	return $mysqli;
}

function qry_log($sqlstr){
	$sqlstr = str_replace("'","\'",$sqlstr);
	$log_sql = "INSERT INTO t_qrylog(sqlstr) VALUES('" . $sqlstr . "')";

	$mysqli = new_mysqli();
	$mysqli -> set_charset("utf8");
	$mysqli->query($log_sql);
	$mysqli->close();
}


function CreateQuery($sqlstr){
	qry_log($sqlstr);
	$mysqli = new_mysqli();
	$mysqli -> set_charset("utf8");
    $result = $mysqli -> query ($sqlstr);
	return $result;

#	$sql_err = $mysqli->error;
#	echo "<br> err=" . $sql_err;
}

function RunQuery($sqlstr){
	qry_log($sqlstr);
	$mysqli = new_mysqli();
	$mysqli -> set_charset("utf8");

	// Check connection
	if ($mysqli->connect_error) {
	    die("Connection failed: " . $mysqli->connect_error);
	}

	if ($mysqli->query($sqlstr)===TRUE){
		return true;
	} else {
		#$gui_data("sql_error") = "Error: " . $sqlstr . "<br>" . $mysqli->error;
		return false;
	}

	$mysqli->close();

}
?>
