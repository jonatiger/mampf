<?php
echo date('Y-m-d',strtotime('2019-05-03 23:59:59'))


?>

include_once "db_connect.inc.php";

$sql_str = "SELECT CASE WHEN
  (SELECT datum FROM t_mampf WHERE mampf_id = 13 AND isCurrent = 1) > (SELECT DATE(StrVal) FROM t_para WHERE item = 'freeze_date')
  AND
  (SELECT datum FROM t_mampf WHERE mampf_id = 13 AND isCurrent = 1) > (SELECT date_add(current_date, interval -3 month))
THEN 1 ELSE 0 END as is_not_frozen";

$mampf_is_not_frozen = mysqli_fetch_array(CreateQuery($sql_str))[0];
if($mampf_is_not_frozen == 1){
  echo "update";
} else {
  echo "no update";
}