<?php
include_once "db_connect.inc.php";

function sql_select_main($startdate, $enddate){
	#dates in format %Y-%m-%d
        $sql_str = "SELECT m.mampf_id, m.datum, m.vorspeise, m.gericht, m.beilage, m.dessert, m.uhrzeit, m.ort, m.kommentar as m_com, m.max_guest, m.deadline, m.koch_user_id, m.kosten,
                t.user_id, u.user_name, t.isHelfer, t.isEinkauf, t.isDessert, t.kommentar as u_com, t.auslage, t.createdate,
                m.veggi
        FROM t_mampf m
        LEFT JOIN t_teilnehmer t ON t.mampf_id = m.mampf_id AND t.isCurrent = 1
        LEFT JOIN t_user u ON u.user_id = t.user_id
        WHERE m.datum BETWEEN '" . $startdate . "' AND '" . $enddate . "'
        AND m.isCurrent = 1
        ORDER BY m.datum desc, t.createdate asc";

        return CreateQuery($sql_str);
}

function get_all_user(){
        $sql_str = "SELECT user_id, user_name FROM t_user WHERE user_status > 0 ORDER BY user_status, user_name";
        return CreateQuery($sql_str);
}

function get_mampf_data($mampf_id){
        $sql_str = "SELECT mampf_id,datum,koch_user_id,vorspeise,gericht,beilage,dessert,uhrzeit,ort,kommentar,max_guest,deadline,lastChange,
                    user_name as koch_name, veggi, max_helfer
                FROM t_mampf m
                INNER JOIN t_user u ON u.user_id = m.koch_user_id
                WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
        return CreateQuery($sql_str);
}

function get_user_data($mampf_id, $user_id){
        $sql_str = "SELECT t.user_id, u.user_name, t.kommentar, t.isKoch, t.isHelfer, t.isEinkauf, t.isDessert, t.auslage
                FROM t_teilnehmer as t
                INNER JOIN t_user u ON u.user_id = t.user_id
                WHERE t.mampf_id = " . $mampf_id . " AND t.isCurrent = 1" . " AND t.user_id =" . $user_id;
        return CreateQuery($sql_str);
}

function get_mampfdate($mampf_id){
        $sql_str = "SELECT datum FROM t_mampf WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
        return CreateQuery($sql_str);
}

function get_balance(){
        $sql_str = "SELECT A.user_name, round(coalesce(C.soll,0)-coalesce(B.haben,0),2) as saldo, coalesce(D.einkauf,'') as einkauf, coalesce(D.helfer,'') as helfer, D.last_mampf, D.mampf_count, A.comment, A.dessert_due
                FROM t_user A
                LEFT JOIN (SELECT haben_konto as user_id, sum(betrag) as haben FROM t_transaction GROUP BY 1
                ) B ON B.user_id = A.user_id
                LEFT JOIN (SELECT soll_konto as user_id, sum(betrag) as soll FROM t_transaction GROUP BY 1
                ) C ON C.user_id = A.user_id
                LEFT JOIN (SELECT user_id, sum(isEinkauf) as einkauf, sum(isHelfer) as helfer, coalesce(max(m.datum),'-') as last_mampf, coalesce(count(distinct m.mampf_id),'') as mampf_count
                        FROM t_teilnehmer t
                        INNER JOIN t_mampf m ON m.mampf_id = t.mampf_id AND m.isCurrent = 1
                        WHERE t.isCurrent = 1 AND m.datum >= '2016-09-01' GROUP BY 1
                ) D ON D.user_id = A.user_id
                WHERE A.user_status IN(0,1,2)
                ORDER BY A.user_status, A.user_name "
                ;
        return CreateQuery($sql_str);
}

function get_user_credit($user_id){
  $sql_str = "SELECT sum((soll_konto = " . $user_id . ") * betrag) - sum((haben_konto = " . $user_id . ") * betrag) as credit
      FROM t_transaction";
  return mysqli_fetch_array(CreateQuery($sql_str))[0];
}

function get_free_tasks($mampf_id, $guest_id){
	$sql_str = "SELECT Sum(isKoch) = 0 as koch, Sum(isHelfer) < m.max_helfer as helfer, Sum(iseinkauf) = 0 as einkauf,
      Sum(isDessert) = 0 as dessert
	  FROM t_teilnehmer t
    INNER JOIN t_mampf m ON m.mampf_id = t.mampf_id AND m.isCurrent = 1
		WHERE mampf_id = " . $mampf_id . " AND user_id <> " . $guest_id . " AND isCurrent = 1";
	$res = mysqli_fetch_array(CreateQuery($sql_str));
	return $res;
}

function guest_exist($mampf_id, $guest_id){
	$sql_str = "SELECT count(*) FROM t_teilnehmer
		WHERE mampf_id = " . $mampf_id . " AND user_id = " . $guest_id . " AND isCurrent = 1";
	$res = (mysqli_fetch_array(CreateQuery($sql_str))[0] > 0);
	#if($guest_id == 99) $res=1;
	return $res;
}

function deadline_expired($mampf_id){
  $sql_str = "SELECT deadline < Now() FROM t_mampf WHERE mampf_id = " . $mampf_id . " AND isCurrent = 1";
  $res = mysqli_fetch_array(CreateQuery($sql_str))[0];
  return $res;
}
?>
