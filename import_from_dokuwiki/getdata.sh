#!/bin/bash

cd data

for i in `seq 2011 2017`;
  do
    wget -O mampf$i.xhtml --no-check-certificate https://mampf.hadiko.de/doku.php?id=mampf_$i\&do=export_xhtmlbody
  done
