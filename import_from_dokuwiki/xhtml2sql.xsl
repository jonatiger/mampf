<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ext="http://exslt.org/common">
    <xsl:strip-space elements="*"/>

    <xsl:variable name="br">
        <xsl:text>&#xa;</xsl:text>
    </xsl:variable>
    <xsl:variable name="sep">
        <xsl:text>;</xsl:text>
    </xsl:variable>

    <!--                  -->
    <!-- Helper functions -->
    <!--                  -->

    <xsl:template name="GetLastSegment">
        <xsl:param name="value"/>
        <xsl:param name="separator" select="'.'"/>

        <xsl:choose>
            <xsl:when test="contains($value, $separator)">
                <xsl:call-template name="GetLastSegment">
                    <xsl:with-param name="value" select="substring-after($value, $separator)"/>
                    <xsl:with-param name="separator" select="$separator"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="clean">
        <xsl:param name="value"/>
        <xsl:param name="isAttr"/>
        <xsl:variable name="vAllowedSymbols"
                      select="'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789öäüßÄÖÜ?._ ,()[]'"/>

        <xsl:variable name="stripped_chars">
            <xsl:value-of select="
                translate(
                $value,
                translate($value, $vAllowedSymbols, ''),
                ' '
                )"/>
        </xsl:variable>
        <xsl:value-of select="normalize-space($stripped_chars)"/>

    </xsl:template>


    <!--                  -->
    <!-- Entry point      -->
    <!--                  -->

    <xsl:template match="/">
        <xsl:variable name="firstPass">
            <xsl:apply-templates/>
        </xsl:variable>
        <xsl:variable name="secondPass">
            <xsl:apply-templates mode="mPass2"
                                 select="ext:node-set($firstPass)/*"/>
        </xsl:variable>

        <xsl:apply-templates mode="mPass3"
                             select="ext:node-set($secondPass)/*"/>

    </xsl:template>

    <xsl:template match="@* | node()">
        <xsl:apply-templates select="@* | node()"/>
    </xsl:template>


    <xsl:template match="node()|@*" mode="mPass2">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="mPass2"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="h1">
        <mampf>
            <metadata>
                <meta>
                    <xsl:attribute name="value">
                        <xsl:value-of select="text()"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">
                        <xsl:text>name</xsl:text>
                    </xsl:attribute>
                </meta>
                <meta>
                    <xsl:attribute name="value">
                        <xsl:variable name="vAllowedSymbols"
                                      select="'0123456789.'"/>

                        <xsl:variable name="value2">
                            <xsl:call-template name="GetLastSegment">
                                <xsl:with-param name="value" select="text()"/>
                                <xsl:with-param name="separator" select="','"/>
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:variable name="value3">
                            <xsl:value-of select="
                            translate(
                            $value2,
                            translate($value2, $vAllowedSymbols, ''),
                            ''
                            )"/>
                        </xsl:variable>
                        <xsl:variable name="value4">
                            <xsl:value-of select="substring($value3,0,11)" />
                        </xsl:variable>
                        <xsl:value-of select="$value4" />
                    </xsl:attribute>
                    <xsl:attribute name="type">
                        <xsl:text>date</xsl:text>
                    </xsl:attribute>
                </meta>
                <xsl:for-each select="following-sibling::div[1]">
                    <xsl:for-each select="ul/li/div">
                        <meta>
                            <xsl:attribute name="value">
                                <xsl:value-of select="text()"/>
                            </xsl:attribute>
                            <xsl:for-each select="strong">
                                <xsl:attribute name="type">
                                    <xsl:value-of select="text()"/>
                                </xsl:attribute>

                            </xsl:for-each>
                        </meta>
                    </xsl:for-each>
                </xsl:for-each>
            </metadata>
            <zusagen>
                <xsl:for-each select="following-sibling::h2[1]">
                    <xsl:for-each select="following-sibling::div[1]">
                        <xsl:for-each select="div/table/tr">
                            <xsl:if test="td[2]/text()!='Leer'">
                                <zusage>
                                    <xsl:attribute name="person">
                                        <xsl:for-each select="td[2]">
                                            <xsl:value-of select="text()"/>
                                        </xsl:for-each>
                                    </xsl:attribute>
                                    <xsl:attribute name="bemerkung">
                                        <xsl:for-each select="td[3]">
                                            <xsl:value-of select="text()"/>
                                        </xsl:for-each>
                                    </xsl:attribute>
                                    <xsl:attribute name="bemerkung2">
                                        <xsl:for-each select="td[4]">
                                            <xsl:value-of select="text()"/>
                                        </xsl:for-each>
                                    </xsl:attribute>
                                </zusage>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:for-each>
                </xsl:for-each>
            </zusagen>
        </mampf>
    </xsl:template>


    <xsl:template match="node()|@*" mode="mPass2">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="mPass2"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="text()" mode="mPass2">
        <xsl:call-template name="clean">
            <xsl:with-param name="value" select="."/>
            <xsl:with-param name="isAttr" select="false()"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="@*" mode="mPass2">
        <xsl:attribute name="{name()}">
            <xsl:call-template name="clean">
                <xsl:with-param name="value" select="."/>
                <xsl:with-param name="isAttr" select="true()"/>

            </xsl:call-template>
        </xsl:attribute>
    </xsl:template>


    <xsl:template match="node()|@*" mode="mPass3">
        <pass3>
            <xsl:apply-templates select="node()|@*" mode="mPass3"/>
        </pass3>
    </xsl:template>

    <xsl:template match="mampf" mode="mPass3">

        <xsl:variable name="name">
            <xsl:for-each select="metadata/meta[@type='name']">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="date">
            <xsl:for-each select="metadata/meta[@type='date']">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="time">
            <xsl:for-each select="metadata/meta[contains(@type, 'hrzeit')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="vorspeise">
            <xsl:for-each select="metadata/meta[contains(@type, 'orspeise')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="hauptgericht">
            <xsl:for-each select="metadata/meta[contains(@type, 'auptgericht')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="beilage">
            <xsl:for-each select="metadata/meta[contains(@type, 'eilage')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="nachtisch">
            <xsl:for-each select="metadata/meta[contains(@type, 'achtisch')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>

        <xsl:variable name="einkaufkosten">
            <xsl:for-each select="metadata/meta[contains(@type, 'inkaufkosten')]">
                <xsl:value-of select="@value"/>
            </xsl:for-each>
        </xsl:variable>


        <xsl:for-each select="zusagen/zusage">

            <xsl:value-of select="translate($name, '>', '')"/>
            <xsl:value-of select="$sep"/>

            <xsl:value-of select="$date"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$time"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$vorspeise"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$hauptgericht"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$beilage"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$nachtisch"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="$einkaufkosten"/>
            <xsl:value-of select="$sep"/>

            <xsl:value-of select="@person"/>
            <xsl:value-of select="$sep"/>
            <xsl:value-of select="@bemerkung"/>
            <xsl:value-of select="@bemerkung2"/>

            <xsl:value-of select="$br"/>

        </xsl:for-each>

    </xsl:template>


</xsl:stylesheet>